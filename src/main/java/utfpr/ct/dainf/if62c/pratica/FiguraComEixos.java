
package utfpr.ct.dainf.if62c.pratica;


public interface FiguraComEixos extends Figura {
    double getEixoMenor();
    public double getEixoMaior();
}
